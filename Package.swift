// swift-tools-version:5.1
import PackageDescription

let package = Package(
    name: "SwiftTestMetrics",
    dependencies: [
    ],
    targets: [
        .target(
            name: "SwiftTestMetrics",
            dependencies: []),
        .testTarget(
            name: "SwiftTestMetricsTests",
            dependencies: ["SwiftTestMetrics"])
    ]
)
