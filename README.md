# Swift Test Metrics

This is a simple one-file program intended to parse the `swift test` code coverage export JSON file.

To use, simply add the following build stage to your `gitlab-ci.yml` file:

```
metrics:
  script:
    - git clone git@gitlab.com:hpux735/gitlab-ci-metrics.git
    - ./gitlab-ci-metrics/Resources/metrics.sh
    - cat metrics.txt
  tags:
    - jazzy
    - swiftlint
  artifacts:
    reports:
      metrics: metrics.txt
```

## Options

There are a small number of options available:

* `-h` Print help/usage
* `-x` Include "checkout" files (all the dependencies of a  project)
* `-m` Print Prometheus metrics output
* `-c` Print a CSV of the summary data; one row per source file

All the files included after the options are processed together.  This may be important for projects with more than one target.  The source filenames are uniqued, so if one source file is in two coverage files, only one will be included in the totals.

## Outputs

Sample output:

### Prometheus

```
test_file_count 15
test_function_count 229
test_function_coverage 88
test_function_percent 0.38427947598253276
test_instantiation_count 229
test_instantiation_coverage 88
test_instantiation_percent 0.38427947598253276
test_line_count 2412
test_line_coverage 1513
test_line_percent 0.6272802653399668
test_region_count 1292
test_region_coverage 654
test_region_percent 0.5061919504643962
```

### CSV

```
Source Filename, Function Count, Function Coverage, Function Percent, Instantation Count, Instantation Coverage, Instantation Percent, Line Count, Line Coverage, Line Percent, Region Count, Region Coverage, Region Percent
"/Users/wdillon/Documents/Source/SwiftTestMetrics/Tests/SwiftTestMetricsTests/SwiftTestMetricsTests.swift", 5, 4, 80.0, 5, 4, 80.0, 39, 37, 94.87179487179486, 9, 7, 77.77777777777779
```