#!/bin/bash -xe

TOP_DIR=$(dirname "${0}")/..
PATH=/opt/local/bin:/usr/local/bin/:$PATH

trap "echo -e '\nError occured in build:'; cat .temp_output.txt" ERR

rm metrics.txt || true

# Errors and warnings

# Make sure that we're building everything from scratch
swift package clean
BUILD="$(swift build 1> .temp_output.txt)"

WARNINGS=$(cat .temp_output.txt| grep "warning"| wc -l)
ERRORS=$(cat .temp_output.txt| grep "error"| wc -l)
NOTES=$(cat .temp_output.txt| grep "note"| wc -l)
rm .temp_output.txt || true

echo "build_errors ${ERRORS}" >> metrics.txt
echo "build_warnings ${WARNINGS}" >> metrics.txt
echo "build_notes ${NOTES}" >> metrics.txt

# Tests and coverage
trap "echo -e '\nError occured in test:'; cat .temp_output.txt" ERR

TEST="$(swift test --enable-code-coverage 2> .temp_output.txt)"

TESTS=$(cat .temp_output.txt| grep "started"| wc -l)
PASSED=$(cat .temp_output.txt| grep "passed"| wc -l)
#rm .temp_output.txt

echo "test_count" ${TESTS} >> metrics.txt
echo "test_passed" ${TESTS} >> metrics.txt

find $TOP_DIR/../.build/debug
$TOP_DIR/Sources/SwiftTestMetrics/main.swift -m $(find $TOP_DIR/../.build/debug/codecov -name "*json") >> metrics.txt || true

# Linting metrics
LINTING="$(swiftlint 2>&1 > /dev/null | tail -n 1)"

# This is super fragile, but what are you going to do? _Learn bash!?_  Obviously not.
# The format of the output in LINTING is "Done linting! Found 10 violations, 0 serious in 2 files."
# Cut the string into words (space-delimited) and take the 4th, 6th, and 9th values.
VIOLATIONS=$(echo $LINTING| cut -d ' ' -f 4)
SERIOUS=$(   echo $LINTING| cut -d ' ' -f 6)
FILES=$(     echo $LINTING| cut -d ' ' -f 9)

echo "lint_violations ${VIOLATIONS}" >> metrics.txt
echo "lint_serious ${SERIOUS}" >> metrics.txt
echo "lint_files ${FILES}" >> metrics.txt

# Documentation metrics
DOCUMENTATION="$(jazzy --no-hide-documentation-coverage | grep 'documentation coverage' || true)"
# Also fragile, but here's the example: "13% documentation coverage with 210 undocumented symbols"
PERCENT=$(echo $DOCUMENTATION| cut -d ' ' -f 1)
SYMBOLS=$(echo $DOCUMENTATION| cut -d ' ' -f 5)

# This one also removes the last character (the '%')
echo "doc_percent ${PERCENT%?}" >> metrics.txt
echo "doc_missing_symbols ${SYMBOLS}" >> metrics.txt

