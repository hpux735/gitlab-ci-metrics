#!/usr/bin/swift

import Foundation

struct Coverage: Decodable, Encodable {
    let data: [CoverageData]
    let type: String
    let version: String
}

struct CoverageData: Encodable, Decodable {
    let files: [File]
//    let functions: [Function]
    let totals: Total
}

struct File: Encodable, Decodable, CustomDebugStringConvertible {
    let filename: String
    let summary: Total

    var debugDescription: String {
        var retval = ""

        retval += "filename: \n\t\(filename)"
        retval += "Summary: \n\t\(summary.debugDescription)"

        return retval
    }
}

struct Total: Encodable, Decodable, CustomDebugStringConvertible {
    var functions: Summary
    var instantiations: Summary
    var lines: Summary
    var regions: Summary

    var debugDescription: String {
        var retval = ""

        retval += "Functions: \n\t\(functions.debugDescription)"
        retval += "Instantiations: \n\t\(instantiations.debugDescription)"
        retval += "Lines: \n\t\(instantiations.debugDescription)"
        retval += "Regions: \n\t\(instantiations.debugDescription)"

        return retval
    }
}

struct Summary: Encodable, Decodable, CustomDebugStringConvertible {
    var count: Int
    var covered: Int
    var percent: Double

    var debugDescription: String {
        return "Count: \(count), Covered: \(covered), Percent: \(percent)"
    }
}

struct Function: Encodable, Decodable {
    let count: Int
    let filenames: [String]
    let name: String
    let regions: [[Int]]
}

let usage = """
Usage: \(CommandLine.arguments[0]) [options] filename

Options:
    -h  - Print this menu
    -x  - Include metrics for checkouts (only the top-level project)

    -m  - Output in promethius metrics format
    -c  - Output in csv format

Filename:
    Must be in Swift Package Manager (SPM) JSON code coverage format
"""

var csv = false
var metrics = false
var checkouts = false
var coverageFiles = [String]()

if CommandLine.argc >= 3 {
    let options = CommandLine.arguments.filter { (arg: String) -> Bool in
        return arg.hasPrefix("-")
    }

    for option in options {
        switch option {
        case "-x": checkouts = true
        case "-c": csv = true
        case "-m": metrics = true
        case "-h", "--help":
            print(usage)
            exit(EXIT_SUCCESS)
        default:
            print(usage)
            exit(EXIT_FAILURE)
        }
    }
}

for arg in CommandLine.arguments[1 ..< CommandLine.arguments.count].filter({
    return !$0.hasPrefix("-")
}) {
    coverageFiles.append(arg)
}

guard !coverageFiles.isEmpty else {
    print("Unable to get filename from command line")
    print(usage)
    exit(EXIT_FAILURE)
}

var sourceFiles = [String: Total]()

for coverageFile in coverageFiles {
    let jsonData: Data!
    do {
        jsonData = try Data(contentsOf: URL(fileURLWithPath: coverageFile))
    } catch {
        print("Unable to open supplied file: \(coverageFile): \(error.localizedDescription)")
        exit(EXIT_FAILURE)
    }

    let coverage: Coverage!
    do {
        let decoder = JSONDecoder()
        coverage = try decoder.decode(Coverage.self, from: jsonData)
    } catch {
        print("Unable to parse code coverage file: \(error.localizedDescription)")
        exit(EXIT_FAILURE)
    }

    var files = [File]()
    if !checkouts {
        for data in coverage.data {
            files += data.files.filter({ (file: File) -> Bool in
                return !file.filename.contains(".build/checkouts")
            })
        }
    } else {
        for data in coverage.data {
            files += data.files
        }
    }

    for file in files {
        // Dont consider test files
        if !file.filename.contains("/Tests/") {
            sourceFiles[file.filename] = file.summary
        }
    }
}

if metrics {
    let functionCount    = sourceFiles.values.map({$0.functions.count   }).reduce(0, +)
    let functionCoverage = sourceFiles.values.map({$0.functions.covered }).reduce(0, +)
    let functionPercent  = Double(functionCoverage) / Double(functionCount)

    let instantiationCount    = sourceFiles.values.map({$0.instantiations.count   }).reduce(0, +)
    let instantiationCoverage = sourceFiles.values.map({$0.instantiations.covered }).reduce(0, +)
    let instantiationPercent  = Double(instantiationCoverage) / Double(instantiationCount)

    let lineCount    = sourceFiles.values.map({$0.lines.count   }).reduce(0, +)
    let lineCoverage = sourceFiles.values.map({$0.lines.covered }).reduce(0, +)
    let linePercent  = Double(lineCoverage) / Double(lineCount)

    let regionCount    = sourceFiles.values.map({$0.regions.count   }).reduce(0, +)
    let regionCoverage = sourceFiles.values.map({$0.regions.covered }).reduce(0, +)
    let regionPercent  = Double(regionCoverage) / Double(regionCount)

    print("test_file_count \(sourceFiles.count)")

    print("test_function_count \(functionCount)")
    print("test_function_coverage \(functionCoverage)")
    print("test_function_percent \(functionPercent)")

    print("test_instantiation_count \(instantiationCount)")
    print("test_instantiation_coverage \(instantiationCoverage)")
    print("test_instantiation_percent \(instantiationPercent)")

    print("test_line_count \(lineCount)")
    print("test_line_coverage \(lineCoverage)")
    print("test_line_percent \(linePercent)")

    print("test_region_count \(regionCount)")
    print("test_region_coverage \(regionCoverage)")
    print("test_region_percent \(regionPercent)")
} else if csv {
// swiftlint:disable:next line_length
    print("Source Filename, Function Count, Function Coverage, Function Percent, Instantation Count, Instantation Coverage, Instantation Percent, Line Count, Line Coverage, Line Percent, Region Count, Region Coverage, Region Percent")

    for (file, total) in sourceFiles {
// swiftlint:disable:next line_length
        print("\"\(file)\", \(total.functions.count), \(total.functions.covered), \(total.functions.percent), \(total.instantiations.count), \(total.instantiations.covered), \(total.instantiations.percent), \(total.lines.count), \(total.lines.covered), \(total.lines.percent), \(total.regions.count), \(total.regions.covered), \(total.regions.percent)")
    }
}
