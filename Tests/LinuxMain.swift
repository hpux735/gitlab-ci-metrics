import XCTest

import SwiftTestMetricsTests

var tests = [XCTestCaseEntry]()
tests += SwiftTestMetricsTests.allTests()
XCTMain(tests)
